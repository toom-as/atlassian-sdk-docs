---
category: devguide
date: '2019-02-05'
platform: server
product: atlassian-sdk
subcategory: updates
title: AMPS 8.0.0
---
# AMPS 8.0.0

### Release Date: February 05, 2019

### Enhancement / Features:

- [AMPS-1476](https://ecosystem.atlassian.net/browse/AMPS-1476}) - Remove maven-stash-plugin and all its associated bits
- [AMPS-1474](https://ecosystem.atlassian.net/browse/AMPS-1474}) - update latest default server product versions
- [AMPS-1470](https://ecosystem.atlassian.net/browse/AMPS-1470}) - Bump Tomcat from 8.5.23 to 8.5.35 in Jira 
- [AMPS-1462](https://ecosystem.atlassian.net/browse/AMPS-1462}) - Starting a product should fail if the specified / default port is in use. 
- [AMPS-1439](https://ecosystem.atlassian.net/browse/AMPS-1439}) - Make AMPS compatible with Java 11

### Bug fixes:

- [AMPS-1473](https://ecosystem.atlassian.net/browse/AMPS-1473}) - product version not being set in POM when creating new plugin
- [AMPS-1429](https://ecosystem.atlassian.net/browse/AMPS-1429}) - Jira logs multiple FileNotFoundExceptions about missing JARs in common/lib
